void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  while(!Serial.available()){}
}

void loop() {
  if(Serial.available())
  {
    char temp = Serial.read();
    if (temp == '\n') return;
    if (temp == 'O'){
      digitalWrite(LED_BUILTIN, LOW);
    }
    else{
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }
}
