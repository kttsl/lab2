#define WAIT 0
#define READ 1

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  while(!Serial.available()){}
}

String str = "";
int state = 0;
uint64_t timer = 0;

void loop() {
  ++timer;
  if(Serial.available()){
    char temp = Serial.read();
    switch(state){
      case WAIT:
        if (temp == 'O'){
          str += 'O';
          state = READ;
        }
        break;
      case READ:
        if (str == "ON" && temp == '\n'){
          digitalWrite(LED_BUILTIN, LOW);
          backWait();
        }
        else if (str == "OFF" && temp == '\n'){
          digitalWrite(LED_BUILTIN, HIGH);
          backWait();
        }
        else if (str == "OF" || str == "O"){}//wait .. read next time
        else{
          backWait();
        }
        if (temp != '\n'){
          str += temp;
        }
      }
  }
  else if (timer > 5000 && state == READ){
    backWait();
  }

  delay(1);
}

void gotoRead(){
  state = READ;
  timer = 0;
}

void backWait(){
  state = WAIT;
  str = "";
  timer = 0;
}
