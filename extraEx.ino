#define WAIT 0
#define READ 1

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  while(!Serial.available()){}
}

String str = "";
int state = 0;
uint64_t timer = 0;

void loop() {
  ++timer;
  if(Serial.available())
  {
    char temp = Serial.read();
    switch(state){
      case WAIT:
        if (temp != '\n' && temp != '#' && temp != '\0'){
          str += temp;
          state = READ;
        }
        break;
      case READ:
        if (timer >= 5000){
          backWait();
        }
        else{
          if (temp == '#'){
            if (isOpen()){
              digitalWrite(LED_BUILTIN, HIGH);
            }
          }
          else if (temp != '\n' && temp != '\0') 
            str += temp;
        }
        break;
        
      default:
        backWait();
    }
  }
  else if (timer >= 5000 && state == READ){
    backWait();
  }

  delay(1);
}

void gotoRead(){
  state = READ;
  timer = 0;
}

void backWait(){
  digitalWrite(LED_BUILTIN, LOW);
  state = WAIT;
  str = "";
  timer = 0;
}

bool isOpen(){
  int len = str.length();
  if (len < 4) return false;
  if (str[len-4] == 'O' && str[len-3] == 'P' && str[len-2] == 'E' && str[len-1] == 'N') 
    return true;
  return false;
}
